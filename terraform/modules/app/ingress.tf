resource "kubernetes_ingress_v1" "app" {
    metadata {
        name        = var.app_name
        namespace   = var.namespace
    }
    spec {
        ingress_class_name = "traefik"
        rule {
            host = var.app_host
            http {
                path {
                    path      = "/"
                    path_type = "Prefix"
                    backend {
                        service {
                            name = var.app_name
                            port {
                                number = 80
                            }
                        }
                    }
                }
            }
        }
    }
}
