resource "kubernetes_deployment" "app" {
    wait_for_rollout = true
    metadata {
        name      = var.app_name
        namespace = var.namespace
        labels    = {
            env = var.app_env
            app = var.app_name
        }
    }

    spec {
        replicas = var.app_replicas

        selector {
            match_labels = {
                env = var.app_env
                app = var.app_name
            }
        }

        template {
            metadata {
                labels = {
                    env = var.app_env
                    app = var.app_name
                }
            }

            spec {
                container {
                    image             = lower(var.app_docker_image)
                    name              = "app"
                    port {
                        container_port = 80
                    }
                    dynamic "env" {
                        for_each = var.app_envs
                        content {
                            name = env.value["name"]
                            value = env.value["value"]
                        }
                    }
                    dynamic "env" {
                        for_each = var.app_envs_from_config_map
                        content {
                            name = env.value["name"]
                            value_from {
                                config_map_key_ref {
                                    key  = env.value["config_map_key"]
                                    name = env.value["config_map_name"]
                                }
                            }
                        }
                    }
                    dynamic "env" {
                        for_each = var.app_envs_from_secret
                        content {
                            name = env.value["name"]
                            value_from {
                                secret_key_ref {
                                    key  = env.value["secret_key"]
                                    name = env.value["secret_name"]
                                }
                            }
                        }
                    }
                    resources {
                        limits = {
                            cpu    = "0.5"
                            memory = "256Mi"
                        }
                        requests = {
                            cpu    = "0.3"
                            memory = "128Mi"
                        }
                    }
                }
                image_pull_secrets {
                    name = "docker-registry"
                }
            }
        }
    }
}
