resource "kubernetes_horizontal_pod_autoscaler" "app" {
  metadata {
    name = "${var.app_name}-hpa"
    namespace = var.namespace
  }
  spec {
    max_replicas = 10
    min_replicas = 2
    metric {
      type = "Resource"
      resource {
        name = "cpu"
        target {
          type = "Utilization"
          average_utilization = 50
        }
      }
    }
  }
}
