resource "kubernetes_config_map" "app" {
    metadata {
        name      = var.app_name
        namespace = var.namespace
    }
    data = {
        env = var.app_env
    }
}
