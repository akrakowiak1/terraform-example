resource "random_password" "app" {
    length  = 16
    special = true
}

data "aws_ssm_parameter" "some_secret" {
    name = "/some/path/to/secret"
}

resource "kubernetes_secret" "app" {
    metadata {
        name      = "app"
        namespace = var.namespace
    }
    data = {
        secret = random_password.app.result
        some_secret = data.aws_ssm_parameter.some_secret.value
    }
}
