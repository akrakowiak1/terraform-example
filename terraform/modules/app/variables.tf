variable "app_docker_image" {
    type = string
}

variable "namespace" {
    type = string
}

variable "app_name" {
    type = string
}

variable "app_env" {
    type = string
}

variable "app_replicas" {
    type = number
}

variable "app_host" {
    type    = string
}

variable "app_envs_from_secret" {
    type = set(
        object({
            name = string
            secret_name = string
            secret_key = string
        })
    )
    default = []
}

variable "app_envs_from_config_map" {
    type = set(
        object({
            name = string
            config_map_name = string
            config_map_key = string
        })
    )
    default = []
}

variable "app_envs" {
    type = set(
        object({
            name = string
            value = string
        })
    )
    default = []
}
