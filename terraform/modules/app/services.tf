resource "kubernetes_service" "app" {
    metadata {
        name      = var.app_name
        namespace = var.namespace
        labels    = {
            app = var.app_name
            env = var.app_env
        }
    }
    spec {
        port {
            port        = 80
            protocol    = "TCP"
            target_port = 80
        }
        selector = {
            app = var.app_name
            env = var.app_env
        }
        type = "ClusterIP"
    }
}
