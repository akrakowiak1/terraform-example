resource "random_password" "mysql_root" {
    length  = 16
    special = true
}

resource "kubernetes_secret" "mysql" {
    depends_on = [random_password.mysql_root]
    metadata {
        name      = "mysql"
        namespace = var.namespace
    }

    data = {
        username            = "root"
        mysql-password = random_password.mysql_root.result
        mysql-root-password = random_password.mysql_root.result
        mysql-replication-password = random_password.mysql_root.result
    }
}

resource "helm_release" "mysql" {
    depends_on        = [kubernetes_secret.mysql]
    name              = "mysql"
    repository        = "https://charts.bitnami.com/bitnami"
    chart             = "mysql"
    namespace         = var.namespace
    dependency_update = true
    cleanup_on_fail   = true
    atomic            = true
    timeout           = 300
    replace           = true
    wait_for_jobs     = true

    set {
        name  = "auth.existingSecret"
        value = "mysql"
    }
    set {
        name  = "auth.database"
        value = var.init_database_name
    }
    set {
        name  = "primary.name"
        value = "master"
    }
    set {
        name  = "secondary.name"
        value = "slave"
    }
    set {
        name  = "architecture"
        value = var.architecture
    }
    set {
        name  = "secondary.replicaCount"
        value = var.secondary_replica_count
    }
}
