variable "namespace" {
    type = string
}

variable "init_database_name" {
    type = string
}

variable "secondary_replica_count" {
    type = number
}

variable "architecture" {
    type = string
    default = "standalone"
}
