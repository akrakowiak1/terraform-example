module "app" {
  source           = "../modules/app"
  app_docker_image = var.app_docker_image
  app_name         = "some-app"
  namespace        = "some-namespace"
  app_replicas     = 1
  app_host         = "some-url.com"
  app_env          = "prod"
  app_envs         = [
    { name = "APP_EXAMPLE_ENV", value = "some-env-var" },
  ]
}
