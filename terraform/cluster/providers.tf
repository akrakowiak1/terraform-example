provider "kubernetes" {
    config_path    = "~/.kube/config"
    config_context = "uat"
}

provider "helm" {
    kubernetes {
        config_path    = "~/.kube/config"
        config_context = "uat"
    }
}

provider "aws" {}
