terraform {
    backend "http" {
    }
    required_providers {
        kubernetes = {
            source  = "hashicorp/kubernetes"
            version = "2.13.1"
        }
        helm = {
            source  = "hashicorp/helm"
            version = "2.5.1"
        }
        random = {
            source  = "hashicorp/random"
            version = "3.2.0"
        }
    }
}
